package com.chewie.firebasetrial;

import android.widget.EditText;
import android.widget.Spinner;

public class artist {

    String artistId;
    String name;
    String genre;



    public artist(String artistId, String name, String genre) {
        this.artistId = artistId;
        this.name = name;
        this.genre = genre;
    }

    public String getArtistId() {
        return artistId;
    }

    public String getName() {
        return name;
    }

    public String getGenre() {
        return genre;
    }
}

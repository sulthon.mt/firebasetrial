package com.chewie.firebasetrial;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class artist_list extends ArrayAdapter {

    private Activity context;
    private List<artist> artistList;

    public artist_list(Activity context, List<artist> artistList){
        super(context, R.layout.list_layout, artistList);
        this.context = context;
        this.artistList =artistList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View ListViewItem = inflater.inflate(R.layout.list_layout, null , true);

        TextView tV_name = ListViewItem.findViewById(R.id.tv_name);
        TextView tV_gen = ListViewItem.findViewById(R.id.tv_gen);

        artist Artist = artistList.get(position);
        tV_name.setText(Artist.getName());
        tV_gen.setText(Artist.getGenre());

        return ListViewItem;
    }
}

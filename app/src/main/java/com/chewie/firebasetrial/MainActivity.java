package com.chewie.firebasetrial;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText name;
    Spinner genre;
    Button btn;
    DatabaseReference db_artist;
    ListView lv;
    List<artist> artistList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db_artist = FirebaseDatabase.getInstance().getReference("artist");
        name = findViewById(R.id.Name);
        genre = findViewById(R.id.genre);
        btn = findViewById(R.id.submit);
        lv = findViewById(R.id.list_view);
        artistList = new ArrayList<>();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addArtist();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        db_artist.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                artistList.clear();
                for (DataSnapshot artistSnap : dataSnapshot.getChildren()){
                    artist Artist = artistSnap.getValue(artist.class);

                    artistList.add(Artist);
                }
                artist_list adapter = new artist_list(MainActivity.this, artistList);

                lv.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addArtist(){
        String nama = name.getText().toString();
        String gen = genre.getSelectedItem().toString();

        if (!TextUtils.isEmpty(nama)){
            String id = db_artist.push().getKey();

            artist Artist = new artist(id, nama, gen);

            db_artist.child(id).setValue(Artist);
            Toast.makeText(this, "Artist Added",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, "Input nama terlebih dahulu!!!",Toast.LENGTH_LONG).show();
        }
    }
}
